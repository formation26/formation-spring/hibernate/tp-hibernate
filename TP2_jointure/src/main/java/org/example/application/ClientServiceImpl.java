package org.example.application;

import org.example.domaine.Client;
import org.example.infrastructure.ClientDao;
import org.example.infrastructure.DaoFactory;

public class ClientServiceImpl implements ClientService{
    ClientDao dao= DaoFactory.createDaoClient();
    @Override
    public void createService(Client c) {

        dao.createToDatabase(c);
    }
}
