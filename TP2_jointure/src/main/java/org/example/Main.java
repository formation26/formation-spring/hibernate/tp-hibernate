package org.example;

import org.example.application.ClientService;
import org.example.application.ClientServiceImpl;
import org.example.application.ServiceFactory;
import org.example.domaine.Adresse;
import org.example.domaine.Client;

public class Main {
    public static void main(String[] args) {

        Adresse adr=new Adresse("31000", "victor hugo");


        Client client=new Client("Dupont", "David",adr);

        ClientService service= ServiceFactory.createClientService();
        service.createService(client);
    }
}