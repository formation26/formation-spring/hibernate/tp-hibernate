package org.example.infrastructure;

import org.example.domaine.Client;

public interface ClientDao {

    void createToDatabase(Client c);
}
