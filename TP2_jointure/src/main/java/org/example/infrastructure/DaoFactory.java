package org.example.infrastructure;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DaoFactory {
   static SessionFactory factory;

   public static ClientDao createDaoClient(){
       return new ClientDaoImpl();
    }

    public  static SessionFactory getSessionFactory(){
        if(factory==null){
            factory=new Configuration().configure("hibernate-cfg.xml").buildSessionFactory();
        }

        return factory;
    }
}
