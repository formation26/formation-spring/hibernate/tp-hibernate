package org.example.infrastructure;

import org.example.domaine.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ClientDaoImpl implements ClientDao{
    @Override
    public void createToDatabase(Client c) {
        SessionFactory factory=DaoFactory.getSessionFactory();

        Session session=factory.openSession();

        session.beginTransaction();
        session.persist(c);
        session.getTransaction().commit();
        session.close();
    }
}
