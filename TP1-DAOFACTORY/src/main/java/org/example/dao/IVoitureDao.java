package org.example.dao;

import org.example.domaine.Voiture;

public interface IVoitureDao {

    void create(Voiture v);

    Voiture findById(int id);

    void update(Voiture v);
}
