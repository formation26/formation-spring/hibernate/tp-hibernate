package org.example.dao;

import org.example.domaine.Voiture;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class VoitureDaoPGImpl implements IVoitureDao{
    @Override
    public void create(Voiture v) {
       //String query="insert into   Client ";

        SessionFactory factory=DaoFactory.getSessionFactory();

        Session session=factory.openSession();

        session.beginTransaction();
        session.persist(v);
        session.getTransaction().commit();

        session.close();

    }

    @Override
    public Voiture findById(int id) {
        SessionFactory factory=DaoFactory.getSessionFactory();
        Session session=factory.openSession();
        Voiture v=session.find(Voiture.class,id);
        session.close();
        return v;
    }

    @Override
    public void update(Voiture v) {
        SessionFactory factory=DaoFactory.getSessionFactory();
        Session session=factory.openSession();

        session.beginTransaction();
        session.merge(v);
        session.getTransaction().commit();
        session.close();
    }
}
