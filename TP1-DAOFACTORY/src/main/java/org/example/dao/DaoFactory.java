package org.example.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DaoFactory {
    static int CHOIX=1;

   static SessionFactory sessionFactory=null;
    public static IVoitureDao createVoitureDao(){
         if(CHOIX==1){
             return new VoitureDaoPGImpl();
         }else{
             return new VoitureDaoOracleImpl();
         }

    }
    public static SessionFactory getSessionFactory(){
       if(sessionFactory==null){
           if(CHOIX==1){
               sessionFactory = new Configuration()
                       .configure("hibernate-cfg.pg.xml")
                       .buildSessionFactory();
           }else{
               sessionFactory = new Configuration()
                       .configure("hibernate-cfg.oracle.xml")
                       .buildSessionFactory();
           }
       }
        return sessionFactory;

    }
}
