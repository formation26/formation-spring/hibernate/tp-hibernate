package org.example.application;

import org.example.dao.DaoFactory;
import org.example.dao.IVoitureDao;
import org.example.dao.VoitureDaoMysqlImpl;
import org.example.domaine.Voiture;

public class VoitureServiceImpl implements IVoitureService{

    IVoitureDao dao= DaoFactory.createVoitureDao();

   // IVoitureDao dao=new VoitureDaoMysqlImpl();
    @Override
    public void create(Voiture v) {
         dao.create(v);
    }

    @Override
    public Voiture findById(int id) {
        return dao.findById(id);
    }
}
