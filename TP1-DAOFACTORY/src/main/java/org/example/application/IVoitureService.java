package org.example.application;

import org.example.domaine.Voiture;

public interface IVoitureService {

    void create(Voiture v);
    Voiture findById(int id);

}
